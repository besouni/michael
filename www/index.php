<?php
session_start();
if (isset($_SESSION["user"])){
    header("location: main.php");
}
require_once "blocks/connect-db.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login/Regsiter</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/custom.css">
    <script src="./assets/js/jquery.min.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="header">
                <h1>Forum</h1>
            </div>
        </div>
    </div>
    <?php
        include "blocks/index-body.php";
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="footer">
                <h1>2023</h1>
            </div>
        </div>
    </div>
</div>
</body>
</html>