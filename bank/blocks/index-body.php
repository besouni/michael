<?php
    if(isset($_POST['r_username'])&& isset($_POST['r_password'])){//true someone wants to register
        $cursor = $MySQLdb->prepare("SELECT * FROM users WHERE username=:username");
        $cursor->execute( array(":username"=>$_POST["r_username"]) );
        //resolve validation problem
        $pattern_username = '/^\S*(?=\S{3,})\S*$/';
        $pattern_username = '/^ \S* (?=\S{3,}) \S* $/';
        // beso123

        //#$@&^!
        $pattern_password = '/^\S*(?=\S{4,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/ ';
//        $pattern_password = '/^ \S* (?=\S{4,}) \S* $/';

//        var_dump($_POST["r_username"]);
//        var_dump("!!!".preg_match($pattern_username, "beso")."!!!");
//        var_dump(12);
//        var_dump(!preg_match($pattern_username, $_POST["r_username"]) or !preg_match($pattern_password, $_POST["r_password"]));
//        (strlen($_POST["r_username"]) < 4) or (strlen($_POST["r_password"]) < 3)
        if(!preg_match($pattern_username, $_POST["r_username"])or !preg_match($pattern_password, $_POST["r_password"])){
            $msg="wrong username or password pattern !!!";
            $msg_style = "alert-danger";
        }else if($cursor->rowCount()){
            $msg="username or password already exists!!!";
            $msg_style = "alert-danger";
        }else {
            $cursor = $MySQLdb->prepare("INSERT INTO users (username, password) value (:username,:password)");
            $cursor->execute(array(":username"=>$_POST["r_username"], ":password"=>$_POST["r_password"]));
            $msg = "Registered successfully!!!";
            $msg_style = "alert-success";
        }

    } else if (isset($_POST['l_username'])&& isset($_POST['l_password'])){
        $cursor = $MySQLdb->prepare("SELECT * FROM users WHERE username=:username AND password=:password");
        $cursor->execute(array(":username"=>$_POST["l_username"], ":password"=>$_POST["l_password"]));

        if($cursor->rowCount()){
            $return_value = $cursor->fetch();
            $_SESSION["user"] = $return_value["username"];
            $_SESSION["user_id"] = $return_value["id"];
            header("Location:main.php");
        }else {
            $msg = "Wrong Username or Password";
            $msg_style = "alert-danger";
        }

    }
?>
<div class="row">
    <div class="col-md-6 col-md-offset-3 log-form">
        <div class="panel panel-default">
            <div class="panel-body" id="login-panel">
                <div class="panel-heading form-title">Login </div>
                <form action="#" method="POST">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="l_email" type="text" class="form-control" name="l_username" placeholder="username">
                    </div>
                    <br>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="l_password" type="password" class="form-control" name="l_password" placeholder="password">
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary btn-block">login</button>
                    <a href="#" id="register" class="switch-link"><i class="glyphicon glyphicon-info-sign"></i>register</a>

                    <?php
                    if (isset($msg))
                    {
                        echo '<div class="alert '.$msg_style.'"><strong>Error: </strong>'.$msg.'</div>';
                    }
                    ?>
                </form>
            </div>
            <div class="panel-body" id="register-panel" hidden>
                <div class="panel-heading form-title">Register Form </div>
                <form action="index.php" method="POST">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="r_username" type="text" class="form-control" name="r_username" placeholder="username">
                    </div>
                    <br>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="r_password" type="password" class="form-control" name="r_password" placeholder="password">
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary btn-block">register</button>
                    <a href="#" id="login" class="switch-link"><i class="glyphicon glyphicon-info-sign"></i>login</a>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $("#register").click(function () {
        $("#login-panel").fadeOut(1000);
        setTimeout(
            function() {
                $("#register-panel").removeAttr("hidden");
            },
            1000);
        $("#register-panel").delay(1000).fadeIn(1000);
    });
    $("#login").click(function () {
        $("#register-panel").fadeOut(1000);
        $("#login-panel").delay(1000).fadeIn(1000);
    });
</script>