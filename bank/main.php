<?php
session_start();
if(!isset($_SESSION["user"])){
    header("location:index.php");
}
$user=$_SESSION["user"];
$user_id=$_SESSION["user_id"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Forum</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/custom-for-account.css">
    <script src="./assets/js/jquery.min.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container header">
        <div class="navbar-header">
            <a class="navbar-brand" href="main.php">Bank System</a>
        </div>
        <ul class="nav navbar-nav" style="float: right">
            <li><a href="#" id="logout">Logout</a></li>
        </ul>
    </div>
</nav>
<div class="container">
    <div class="row" id="page">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading font-weight-bold p-3">Account Info</div>
                <div class="panel-body">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <p>
                                <kbd>username:</kbd>
                                <span style="float:right;">
                                    <?php echo $user; ?>
                                </span>
                            </p>
                        </li>
                        <li class="list-group-item">
                            <p>
                                <kbd>ip address</kbd>
                                <span style="float:right;">
                                    <?php echo $_SERVER["REMOTE_ADDR"];?>
                                </span>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading font-weight-bold p-3">Additional Task</div>
                <div class="panel-body">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="javascript: void(0)" id="private-forum">Users</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading font-weight-bold p-3" id="title-of-page">
                    Transactions:
                </div>
                <div class="panel-body">
                    <ul class="list-group" id="post_history">
                    </ul>
                </div>
            </div>
<!--                <div class="input-group" id="send-post">-->
<!--                    <input id="msg" type="text" class="form-control mr-3" name="msg" placeholder="Write your message here...">-->
<!--                    <span class="input-group-addon"><button class="btn-primary" id="send_post">Send</button></span>-->
<!--                </div>-->
        </div>
    </div>
</div>
<div class="container footer">
    2023
</div>
<script>
    $("#logout").click(function() {
        $.post("api.php",{"action":"logout"},function(data)
        {
            if(data.success == "true")
            {
                location.href = "index.php";
            }
        });
     });

    // $("#send_post").click(function(){
    //     $.post("api.php",{"action":"new_post","data":$("#msg").val()},function(data)
    //     {
    //         if(data.success == "true")
    //         {
    //             location.reload();
    //         }
    //     });
    //  });
        
    $.post("api.php",{"action":"get_all_trans"},function(data)
    {
        if(data.success == "true")
        {
            $("#post_history").html(data.data);//data that returns fill post_history
        }

    });

    $("#private-forum").click(function(){
        $("#title-of-page").html("Users:");
        $("#send-post").hide();
        $.post("new-api.php",{"action":"get_all_users"},function(data)
        {
            if(data.success == "true")
            {
                $("#post_history").html(data.data);//data that returns fill post_history
            }

        });
    });
</script>
</body>
</html>
