<?php
session_start();
if(!isset($_SESSION["user"]))
{
    header("location:index.php");
}
$user=$_SESSION["user"];
$user_id=$_SESSION["user_id"];

require_once "blocks/connect-db.php";

$action=$_POST["action"];

if (isset($_POST["data"]))
{
    $data = $_POST["data"];
}

    header('Content-Type: application/json');

    switch($action)
    {
        case "get_all_users":
            $cursor = $MySQLdb->prepare("SELECT * FROM users");
            $cursor->execute();
            $retval="";
            
            foreach($cursor->fetchAll() as $row )
            {
                $retval .=  "<div class='media'>";
                $retval .=         "<div style='width: 30%'><span style='font-weight: bold'>User: </span>".$row['username']."</div>";
                $retval .=  "</div>";
            }
            echo '{"success":"true","data":"'.$retval.'"}';
            break;

        default:
            echo '{"success":"false"}';
            die();
    
    }
?>

